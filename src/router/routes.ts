export const constantRoute = [
  {
    path: '/login',
    component: () => import('@/pages/login/index.vue'),
    name: 'Login',
    meta: {
      title: '登录', //菜单标题
      hidden: true, //代表路由标题在菜单中是否隐藏  true:隐藏 false:不隐藏
      icon: 'Promotion' //菜单文字左侧的图标,支持element-plus全部图标
    }
  },
  {
    path: '/map',
    component: () => import('@/pages/map/index.vue'),
    name: 'Map',
    meta: {
      title: '地图', //菜单标题
      hidden: true, //代表路由标题在菜单中是否隐藏  true:隐藏 false:不隐藏
      icon: 'Promotion' //菜单文字左侧的图标,支持element-plus全部图标
    }
  },
];
